// Requiring basic dependencies

const express = require('express');
const port = process.env.port || 3001;
const app = express();
// To support Json format
app.use(express.json());
// Modularized DB connection
require('./db/connector.js');

//Importing the Tabs model which deals with CRUD of Tabs Collection in Mongo
const tabsModel = require('./models/tabsModel');


/**
 * GET request for listing all tabs
 */
app.get('/tabs', async (req, res) => {

    try {
        // Get all the tabs data
        const resp = await tabsModel.find();
        res.status(200).send(resp);
    } catch (e) {
        res.send(e);
    }
});

/**
 * POST request for Tabs creation
 */
app.post('/tabs', async (req, res) => {
    //Create new data in the collection as document
    const resp = await new tabsModel(req.body);

    //Later saving it in DB after promise is returned
    resp.save().then(resp => {
        res.status(201).send(resp);
    }).catch(err => {
        res.status(400).send(err);
    });
});

/**
 * PUT request for the ammendment of Tabs
 */
app.put('/tabs/:tabId', async (req, res) => {
    try {
        const tabId = req.params.tabId; //Requesting ID from params
        const response = await tabsModel.findByIdAndUpdate({_id: tabId}, req.body, {new: true});
        //If Id doesn't persist in tab or document then send 400 error response
        if (!tabId) {
            res.status(400).send();
        } else {
            //Returns the update response not the older one after update
            res.send(response);
        }
    } catch (e) {
        res.status(400).send(e);
    }
});

/**
 * Delete request for the deletion of tabs
 */
app.delete('/tabs/:tabId', async (req, res) => {
    try {
        //Fetching tab id from params
        const tabId = req.params.tabId;
        const response = await tabsModel.findByIdAndDelete(tabId);
        if (!tabId) {
            res.status(400).send();
        } else {
            res.send(response);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

/**
 * GET api for the tab stats (OPTIONAL TASK)
 * return the count of DataPoints with the name of Tabs
 */
app.get('/tabStats', async (req, res) => {
    try {
        //Aggregrate Query for the Datapoints counting using Project and Size
        const response = await tabsModel.aggregate([{
            $project: {
                name: 1,
                _id: 0,
                dataPointCount: {$size: "$dataPoints"}
            }
        }]);
        res.send(response);
    } catch (e) {
        res.send(e);
    }
});
app.listen(port);
