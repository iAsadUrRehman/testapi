const mongoose = require('mongoose');

const homeSchema = new mongoose.Schema({
    'test' : 'string'
});

const homeModel = new mongoose.model('home', homeSchema);

module.exports = homeModel;
