const mongoose = require('mongoose');
const validator = require('validator');

const dataPointsSchema = new mongoose.Schema({
   'dataType': {
       type:String,
       enum: ['selection', 'text', 'number', 'date'],
       required: true,
       default: 'text'
   },
    'label':{
       type:String,
        required:true,
        uppercase: true
    },
    'description' :{
       type:String,
        required:true,
    },
    'options':{
       type: [0,1,2,3,4,5, 'unknown'],
    },
    'placeholder': String
});

const tabSchema = new mongoose.Schema({
    'name' : {
        type: String,
        required: true,
    },
    'description': {
        type:String,
        required:true,
    },
    'dataPoints': [dataPointsSchema]
});

const tabModel = new mongoose.model('Tab', tabSchema);

module.exports = tabModel;
